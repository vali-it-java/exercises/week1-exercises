package com.valiit;

import java.util.*;

public class Exercises {

    static final int PATTERN_LENGTH = 6;
    static final int PATTERN_LENGTH_2 = 5;

    public static void main(String[] args) {

        // Exercise 1
        System.out.println("\nExercise 1:\n");

        for (int rowNum = 1; rowNum <= PATTERN_LENGTH; rowNum++) {

            for (int colNum = 1; colNum <= PATTERN_LENGTH; colNum++) {
                if (colNum >= rowNum) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        // Exercise 2
        System.out.println("\nExercise 2:\n");

        for (int rowNum = 1; rowNum <= PATTERN_LENGTH; rowNum++) {

            for (int colNum = 1; colNum <= rowNum; colNum++) {
                System.out.print("#");
            }
            System.out.println();
        }

        // Exercise 3
        System.out.println("\nExercise 3:\n");

        for (int rowNum = 1; rowNum <= PATTERN_LENGTH_2; rowNum++) {

            for (int colNum = 1; colNum <= PATTERN_LENGTH_2; colNum++) {
//                if (colNum > PATTERN_LENGTH_2 - rowNum) {
//                    System.out.print("@");
//                } else {
//                    System.out.print(" ");
//                }
                System.out.print(colNum > PATTERN_LENGTH_2 - rowNum ? "@" : " ");
            }
            System.out.println();
        }

        // Exercise 4
        System.out.println("\nExercise 4:\n");

        // Solution 1
        int num1 = 1234567;
        String num1text = String.valueOf(num1);
        String num1ResultText = "";
        for (int i = 0; i < num1text.length(); i++) {
            num1ResultText = num1text.charAt(i) + num1ResultText;
        }
        int num1Result = Integer.parseInt(num1ResultText);
        System.out.println(num1Result);

        // Solution 2
        int num2 = 389;
        String num2text = String.valueOf(num2);
        String num2ResultText = new StringBuilder(num2text).reverse().toString();
        int num2Result = Integer.parseInt(num2ResultText);
        System.out.println(num2Result);

        // Exercise 5
        System.out.println("\nExercise 5:\n");

        int score = Integer.parseInt(args[0]);
        if (score < 51) {
            System.out.println("FAIL");
        } else {
            int grade;
            if (score >= 91) {
                grade = 5;
            } else if (score >= 81) {
                grade = 4;
            } else if (score >= 71) {
                grade = 3;
            } else if (score >= 61) {
                grade = 2;
            } else {
                grade = 1;
            }
            System.out.printf("PASS - %d, %d", grade, score);
            System.out.println();
        }

        // Exercise 6
        System.out.println("\nExercise 6:\n");

        double[][] numberPairs = {{1.3, 2.6}, {6.0, 7.89}, {0.3, 5.32}, {3.0, 4.0}};
        for (double[] numberPair : numberPairs) {
            double result = Math.sqrt(Math.pow(numberPair[0], 2) + Math.pow(numberPair[1], 2));
            System.out.println(result);
        }

        // Exercise 7
        System.out.println("\nExercise 7:\n");

        String[][] countryInfoList = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Finland", "Helsinki", "Antti Rinne"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Russia", "Moscow", "Dmitry Medvedev"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Iceland", "Reykjavík", "Katrín Jakobsdóttir"},
                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
                {"Poland", "Warsaw", "Mateusz Morawiecki"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"}
        };

        for (String[] countryInfo : countryInfoList) {
            System.out.println(countryInfo[2]);
        }

        for (String[] countryInfo : countryInfoList) {
            System.out.printf("Country: %s, Capital: %s, Prime minister: %s\n", countryInfo[0], countryInfo[1], countryInfo[2]);
        }

        // Exercise 8
        System.out.println("\nExercise 8:\n");

        Object[][] countryInfoList2 = {
                {"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš", new String[]{"Latvian", "Russian", "Belarusian", "Ukrainians"}},
                {"Finland", "Helsinki", "Antti Rinne", new String[]{"Finnish", "Swedish", "Sámi"}},
                {"Sweden", "Stockholm", "Stefan Löfven", new String[]{"Swedish", "Finnish", "Yiddish", "Meänkieli", "Romani", "Sami"}},
                {"Russia", "Moscow", "Dmitry Medvedev", new String[]{"Russian", "English", "Tatar", "German"}},
                {"Norway", "Oslo", "Erna Solberg", new String[]{"Norwegian", "Finnish", "Sami", "Arabic"}},
                {"Iceland", "Reykjavík", "Katrín Jakobsdóttir", new String[]{"Icelandic"}},
                {"Lithuania", "Vilnius", "Saulius Skvernelis", new String[]{"Lithuanian", "Polish", "Ukrainian", "Russian", "Belarusian"}},
                {"Poland", "Warsaw", "Mateusz Morawiecki", new String[]{"Polish", "Ukrainian", "Lithuanian", "Russian"}},
                {"Denmark", "Copenhagen", "Mette Frederiksen", new String[]{"Danish", "German"}}
        };

        for (Object[] countryInfo : countryInfoList2) {
            System.out.println(countryInfo[0]);
            System.out.println(String.join(", ", (String[]) countryInfo[3]));
            System.out.println();
        }

        // Exercise 9
        System.out.println("\nExercise 9:\n");

        List<Object[]> countryInfoList3 = new ArrayList<>();
        countryInfoList3.add(new Object[]{"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}});
        countryInfoList3.add(new Object[]{"Latvia", "Riga", "Arturs Krišjānis Kariņš", new String[]{"Latvian", "Russian", "Belarusian", "Ukrainians"}});
        countryInfoList3.add(new Object[]{"Finland", "Helsinki", "Antti Rinne", new String[]{"Finnish", "Swedish", "Sámi"}});
        countryInfoList3.add(new Object[]{"Sweden", "Stockholm", "Stefan Löfven", new String[]{"Swedish", "Finnish", "Yiddish", "Meänkieli", "Romani", "Sami"}});
        countryInfoList3.add(new Object[]{"Russia", "Moscow", "Dmitry Medvedev", new String[]{"Russian", "English", "Tatar", "German"}});
        countryInfoList3.add(new Object[]{"Norway", "Oslo", "Erna Solberg", new String[]{"Norwegian", "Finnish", "Sami", "Arabic"}});
        countryInfoList3.add(new Object[]{"Iceland", "Reykjavík", "Katrín Jakobsdóttir", new String[]{"Icelandic"}});
        countryInfoList3.add(new Object[]{"Lithuania", "Vilnius", "Saulius Skvernelis", new String[]{"Lithuanian", "Polish", "Ukrainian", "Russian", "Belarusian"}});
        countryInfoList3.add(new Object[]{"Poland", "Warsaw", "Mateusz Morawiecki", new String[]{"Polish", "Ukrainian", "Lithuanian", "Russian"}});
        countryInfoList3.add(new Object[]{"Denmark", "Copenhagen", "Mette Frederiksen", new String[]{"Danish", "German"}});

        for (Object[] countryInfo : countryInfoList3) {
            System.out.println(countryInfo[0]);
            System.out.println(String.join(", ", (String[]) countryInfo[3]));
            System.out.println();
        }

        // Exercise 10
        System.out.println("\nExercise 10:\n");

        Map<String, Object[]> countryInfoMap = new HashMap<>();
        countryInfoMap.put("Estonia", new Object[]{"Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}});
        countryInfoMap.put("Latvia", new Object[]{"Riga", "Arturs Krišjānis Kariņš", new String[]{"Latvian", "Russian", "Belarusian", "Ukrainians"}});
        countryInfoMap.put("Finland", new Object[]{"Helsinki", "Antti Rinne", new String[]{"Finnish", "Swedish", "Sámi"}});
        countryInfoMap.put("Sweden", new Object[]{"Stockholm", "Stefan Löfven", new String[]{"Swedish", "Finnish", "Yiddish", "Meänkieli", "Romani", "Sami"}});
        countryInfoMap.put("Russia", new Object[]{"Moscow", "Dmitry Medvedev", new String[]{"Russian", "English", "Tatar", "German"}});
        countryInfoMap.put("Norway", new Object[]{"Oslo", "Erna Solberg", new String[]{"Norwegian", "Finnish", "Sami", "Arabic"}});
        countryInfoMap.put("Iceland", new Object[]{"Reykjavík", "Katrín Jakobsdóttir", new String[]{"Icelandic"}});
        countryInfoMap.put("Lithuania", new Object[]{"Vilnius", "Saulius Skvernelis", new String[]{"Lithuanian", "Polish", "Ukrainian", "Russian", "Belarusian"}});
        countryInfoMap.put("Poland", new Object[]{"Warsaw", "Mateusz Morawiecki", new String[]{"Polish", "Ukrainian", "Lithuanian", "Russian"}});
        countryInfoMap.put("Denmark", new Object[]{"Copenhagen", "Mette Frederiksen", new String[]{"Danish", "German"}});

        for (String key : countryInfoMap.keySet()) {
            System.out.println(key);
            System.out.println(String.join(", ", (String[]) countryInfoMap.get(key)[2]));
            System.out.println();
        }

        // Exercise 11
        System.out.println("\nExercise 11:\n");

        Queue<Object[]> countryInfoQueue = new LinkedList<>();
        countryInfoQueue.add(new Object[]{"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}});
        countryInfoQueue.add(new Object[]{"Latvia", "Riga", "Arturs Krišjānis Kariņš", new String[]{"Latvian", "Russian", "Belarusian", "Ukrainians"}});
        countryInfoQueue.add(new Object[]{"Finland", "Helsinki", "Antti Rinne", new String[]{"Finnish", "Swedish", "Sámi"}});
        countryInfoQueue.add(new Object[]{"Sweden", "Stockholm", "Stefan Löfven", new String[]{"Swedish", "Finnish", "Yiddish", "Meänkieli", "Romani", "Sami"}});
        countryInfoQueue.add(new Object[]{"Russia", "Moscow", "Dmitry Medvedev", new String[]{"Russian", "English", "Tatar", "German"}});
        countryInfoQueue.add(new Object[]{"Norway", "Oslo", "Erna Solberg", new String[]{"Norwegian", "Finnish", "Sami", "Arabic"}});
        countryInfoQueue.add(new Object[]{"Iceland", "Reykjavík", "Katrín Jakobsdóttir", new String[]{"Icelandic"}});
        countryInfoQueue.add(new Object[]{"Lithuania", "Vilnius", "Saulius Skvernelis", new String[]{"Lithuanian", "Polish", "Ukrainian", "Russian", "Belarusian"}});
        countryInfoQueue.add(new Object[]{"Poland", "Warsaw", "Mateusz Morawiecki", new String[]{"Polish", "Ukrainian", "Lithuanian", "Russian"}});
        countryInfoQueue.add(new Object[]{"Denmark", "Copenhagen", "Mette Frederiksen", new String[]{"Danish", "German"}});

        while (countryInfoQueue.size() > 0) {
            Object[] countryInfo = countryInfoQueue.remove();
            System.out.println(countryInfo[0]);
            System.out.println(String.join(", ", (String[]) countryInfo[3]));
            System.out.println();
        }
    }
}
