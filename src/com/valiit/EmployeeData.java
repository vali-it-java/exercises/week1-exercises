package com.valiit;

public class EmployeeData {
    public String firstName;
    public String lastName;
    public int age;
    public String profession;
    public String citizenship;

    public EmployeeData(String data) {
        String[] dataParts = data.split(", ");
        this.firstName = dataParts[0].split(": ")[1];
        this.lastName = dataParts[1].split(": ")[1];
        this.age = Integer.parseInt(dataParts[2].split(": ")[1]);
        this.profession = dataParts[3].split(": ")[1];
        this.citizenship = dataParts[4].split(": ")[1];
    }

    @Override
    public String toString() {
        return String.format("[Nimi: %s %s / vanus: %s / amet: %s / kodakondsus: %s]\n",
                this.firstName, this.lastName, this.age, this.profession, this.citizenship);
    }
}
