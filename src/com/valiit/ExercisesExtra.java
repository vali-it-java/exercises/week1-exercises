package com.valiit;

import java.util.*;
import java.util.stream.Collectors;

public class ExercisesExtra {

    public static void main(String[] args) {

        // Exercise 1
        System.out.println("\nExercise 1:\n");

        // Pattern 1;
        for (int row = 1; row <= 8; row++) {
            for (int col = 1; col <= 19; col++) {
                if (row % 2 == 1) {
                    System.out.print(col % 2 == 1 ? "#" : "+");
                } else {
                    System.out.print(col % 2 == 1 ? "+" : "#");
                }
            }
            System.out.println();
        }
        System.out.println();

        // Pattern 2
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j <= 6; j++) {
                if (j == i) {
                    System.out.print("#");
                } else if (j < i) {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
        System.out.println();

        // Pattern 3
        for (int row = 1; row <= 5; row++) {
            for (int col = 1; col <= 7; col++) {
                if (row % 2 == 0) {
                    System.out.print("=");
                } else {
                    System.out.print(col % 3 == 1 ? "#" : "+");
                }
            }
            System.out.println();
        }

        // Exercise 2

        String personsStr = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA;";

        // Arrays
        String[] persons = (personsStr + " ").split("; ");
        String[][] personsData = new String[persons.length][5];
        for (int personIndex = 0; personIndex < personsData.length; personIndex++) {
            String[] personDetails = persons[personIndex].split(", ");
            for (int personProperty = 0; personProperty < 5; personProperty++) {
                personsData[personIndex][personProperty] = personDetails[personProperty].split(": ")[1];
            }
        }

        for (int i = 0; i < personsData.length; i++) {
            System.out.printf("Nimi: %s %s / vanus: %s / amet: %s / kodakondsus: %s\n",
                    personsData[i][0], personsData[i][1], personsData[i][2], personsData[i][3], personsData[i][4]);
        }

        // Lists (with streams)
        List<List<String>> personsList = Arrays.asList((personsStr + " ").split("; "))
                .stream().map(p -> {
                    return Arrays.asList(p.split(", "))
                            .stream().map(v -> v.split(": ")[1]).collect(Collectors.toList());
                }).collect(Collectors.toList());

        // Lists ("classical" way)
        List<String> personStrList = Arrays.asList((personsStr + " ").split("; "));
        List<List<String>> personsList2 = new ArrayList<>();
        for (int i = 0; i < personStrList.size(); i++) {
            String[] personProperties = personStrList.get(i).split(", ");
            List<String> personPropertiesList = new ArrayList<>();
            for (int j = 0; j < personProperties.length; j++) {
                personPropertiesList.add(personProperties[j].split(": ")[1]);
            }
            personsList2.add(personPropertiesList);
        }

        // Maps
        List<String> personStrList2 = Arrays.asList((personsStr + " ").split("; "));
        List<Map<String, String>> personsListOfMaps = new ArrayList<>();
        for (int i = 0; i < personStrList2.size(); i++) {
            String[] personProperties = personStrList2.get(i).split(", ");
            Map<String, String> personsMap = new HashMap<>();
            for (int j = 0; j < personProperties.length; j++) {
                String[] personPropArr = personProperties[j].split(": ");
                personsMap.put(personPropArr[0], personPropArr[1]);
            }
            personsListOfMaps.add(personsMap);
        }

        for (int i = 0; i < personsListOfMaps.size(); i++) {
            System.out.printf("Nimi: %s %s / vanus: %s / amet: %s / kodakondsus: %s\n",
                    personsListOfMaps.get(i).get("Eesnimi"), personsListOfMaps.get(i).get("Perenimi"),
                    personsListOfMaps.get(i).get("Vanus"), personsListOfMaps.get(i).get("Amet"),
                    personsListOfMaps.get(i).get("Kodakondsus"));
        }

        // Custom Class (EmployeeData)
        String[] listOfEmployeeStrings = (personsStr + " ").split("; ");
        List<EmployeeData> listOfEmployees = new ArrayList<>();
        for(int i = 0; i < listOfEmployeeStrings.length; i++) {
            listOfEmployees.add(new EmployeeData(listOfEmployeeStrings[i]));
        }

        for(int i = 0; i < listOfEmployees.size(); i++) {
            System.out.println(listOfEmployees.get(i));
        }
    }
}









